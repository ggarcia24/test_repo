This file is supposed to describe the project information that 
you will like to have

- Small description of the project

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque 
placerat quam nec risus tincidunt lobortis pellentesque sapien scelerisque. 
Nam molestie volutpat fringilla. Lorem ipsum dolor sit amet, consectetur 
adipiscing elit. 

- How to install it
Curabitur aliquet odio quis urna ornare volutpat. Aliquam 
accumsan, quam sit amet feugiat gravida, purus quam tristique leo, eget 
vestibulum velit dui sed justo. Aenean eget magna odio, quis facilisis 
erat. Integer lacinia ornare mi ultricies facilisis. In in dui in sem 
pulvinar ultrices. Nunc vitae erat a arcu semper viverra eget ac nibh.

- Who/Where to find solutions if you have any issues

Nam feugiat pharetra orci, eu porta diam vestibulum pharetra. Quisque 
gravida, velit a fermentum ornare, leo nunc consequat eros, vel accumsan 
quam nisi a orci.

- Test part added

Nam feugiat pharetra orci, eu porta diam vestibulum pharetra. Quisque 
gravida, velit a fermentum ornare, leo nunc consequat eros, vel accumsan 
quam nisi a orci.

- This is a second test

Curabitur aliquet odio quis urna ornare volutpat. Aliquam 
accumsan, quam sit amet feugiat gravida, purus quam tristique leo, eget 
vestibulum velit dui sed justo.

- Four test added 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque 
placerat quam nec risus tincidunt lobortis pellentesque sapien scelerisque. 
Nam molestie volutpat fringilla.

- Algo

This is just a simple test
